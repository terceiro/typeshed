## 1.17.6 (2022-04-16)

Use `TypeAlias` where possible for type aliases (#7630)

## 1.17.4 (2021-10-12)

Add star to all non-0.1 versions (#6146)

