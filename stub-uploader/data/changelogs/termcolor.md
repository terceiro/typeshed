## 1.1.4 (2022-04-27)

Drop support for Python 2 in termcolor (#7707)

## 1.1.2 (2021-10-12)

Add star to all non-0.1 versions (#6146)

