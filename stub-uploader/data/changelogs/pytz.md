## 2021.3.8 (2022-05-07)

Import generics from standard modules in all third-party stubs (#7791)

## 2021.3.7 (2022-04-27)

Drop Python 2 support in third-party stubs (#7703)

## 2021.3.6 (2022-03-19)

pytz: utcoffset only returns None if dt is None (#7510)

## 2021.3.5 (2022-02-13)

Fix argument types of pytz.tzinfo.StaticTzInfo (#7184)

The argument is_dst of the functions StaticTzInfo.localize and
StaticTzInfo.normalize are ignored, and only present for compatibility with
DstTzInfo. The functions in DstTzInfo also accepts None, so for compatibility,
StaticTzInfo should accept them as well.

[^1] https://github.com/stub42/pytz/blob/2ed682a7c4079042f50975970fc4f503c8450058/src/pytz/tzinfo.py#L112

## 2021.3.3 (2021-12-14)

Add abstract methods to BaseTzInfo (#6579)

While these abstract methods don't exist at runtime, all sub-classes of
BaseTzInfo implement them. It can be useful to annotate variables with
BaseTzInfo and being able to call these methods on it.

## 2021.3.2 (2021-12-09)

pytz: rework stubs (#6551)

## 2021.3.1 (2021-11-23)

Reduce use of deprecated `typing` aliases (#6358)

## 2021.3.0 (2021-10-12)

Add star to all non-0.1 versions (#6146)

