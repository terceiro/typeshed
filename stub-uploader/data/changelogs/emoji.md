## 1.7.1 (2022-06-10)

Add stub for emoji.is_emoji (#8053)

## 1.7.0 (2022-05-22)

Update emoji stubs for version 1.7 (#7884)

## 1.2.8 (2022-04-27)

Drop Python 2 support from emoji (#7716)

## 1.2.6 (2021-10-15)

Use lowercase tuple where possible (#6170)

## 1.2.5 (2021-10-12)

Add star to all non-0.1 versions (#6146)

