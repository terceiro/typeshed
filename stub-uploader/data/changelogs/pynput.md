## 1.7.1 (2022-04-01)

Third-party stubs: Improve several `__exit__` methods (#7575)

## 1.7.0 (2022-02-24)

Add stubs for `pynput` package (#7177)

Fixes #4328

