## 2.4.2 (2022-04-20)

Use `TypeAlias` for type aliases where possible, part II (#7667)

## 2.4.1 (2022-04-16)

Use `TypeAlias` where possible for type aliases (#7630)

## 2.4.0 (2022-02-11)

Add stubs for pyflakes (#7175)

