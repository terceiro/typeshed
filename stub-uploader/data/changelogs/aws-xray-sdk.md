## 2.8.5 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 2.8.4 (2022-02-03)

Improve `__enter__` & constructor methods (#7114)

## 2.8.2 (2021-10-12)

Add star to all non-0.1 versions (#6146)

## 2.8.1 (2021-09-13)

Add more annotations to aws_xray_sdk.core.recorder (#6029)

