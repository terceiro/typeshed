## 0.5.2 (2022-06-01)

`dj-database-url`: Add missing fields to `_DBConfig` TypedDict (#8008)

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>

## 0.5.1 (2022-05-29)

dj_database_url: make all keys optional in _DBConfig TypedDict (#7979)

Co-authored-by: Xavier Francisco <xavier.n.francisco@gmail.com>

## 0.5.0 (2022-05-28)

Add stubs for dj-database-url (#7972)

Co-authored-by: Xavier Francisco <xavier.n.francisco@gmail.com>

