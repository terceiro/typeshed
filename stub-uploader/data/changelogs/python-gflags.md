## 3.1.5 (2022-04-27)

Drop Python 2 support from python-gflags (#7709)

## 3.1.4 (2022-01-28)

Do not use `True` or `False` as default values in assignments (#7060)

## 3.1.3 (2022-01-20)

Remove nearly all `__str__` and `__repr__` methods from typeshed (#6968)

## 3.1.1 (2021-10-12)

Add star to all non-0.1 versions (#6146)

## 3.1.0 (2021-09-19)

Update third-party package versions (#5996)

