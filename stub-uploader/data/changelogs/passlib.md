## 1.7.5 (2022-04-05)

Mark many attributes as read-only properties (#7591)

## 1.7.4 (2022-03-27)

passlib: Annotate various handler methods and fields (#7521)

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>

## 1.7.3 (2022-03-19)

Add mypy error codes to `type: ignore`s, remove unused ignores (#7504)

Co-authored-by: Jelle Zijlstra <jelle.zijlstra@gmail.com>

## 1.7.2 (2022-03-14)

passlib: Annotate pbkdf2_sha* (#7486)

## 1.7.1 (2022-03-13)

passlib: fix MutableMapping import (#7479)

Found by #7478

## 1.7.0 (2022-03-07)

Add passlib stubs (#7024)

