## 1.2.5 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 1.2.4 (2022-03-24)

DateTimeRange: __contains__ accepts datetime.datetime (#7541)

## 1.2.3 (2022-02-07)

Improve some in-place BinOp methods (#7149)

## 1.2.1 (2022-01-02)

Never explicitly inherit from `object` in Python 3-only stubs (#6777)

## 1.2.0 (2021-10-12)

Update remaining versions for third-party stubs (#6094)

Also remove the python2 markers of packages that don't list Python 2
as supported in the latest version.

Don't special case version '0.1'

Co-authored-by: Akuli <akuviljanen17@gmail.com>

