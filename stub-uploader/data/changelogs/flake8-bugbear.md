## 21.11.29.2 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 21.11.29.0 (2021-12-08)

Add stubs for flake8-bugbear (#6543)

