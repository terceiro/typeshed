## 1.5.6 (2022-05-26)

Third-party stubs: fix several fictitious type aliases (#7958)

## 1.5.5 (2022-05-21)

Simplify and correct many numeric unions (#7906)

Unblocks PyCQA/flake8-pyi#222

## 1.5.4 (2022-04-20)

Use `TypeAlias` for type aliases where possible, part II (#7667)

## 1.5.3 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 1.5.1 (2021-12-28)

Use PEP 585 syntax wherever possible (#6717)

## 1.5.0 (2021-11-12)

Bump typed-ast version, recommend Python 3.8 for tests (#6278)

## 1.4.5 (2021-10-12)

Add star to all non-0.1 versions (#6146)

