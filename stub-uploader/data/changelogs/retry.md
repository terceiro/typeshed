## 0.9.7 (2022-05-07)

Import generics from standard modules in all third-party stubs (#7791)

## 0.9.6 (2022-04-27)

Drop Python 2 support in third-party stubs (#7703)

## 0.9.5 (2022-01-08)

Use lowercase `type` everywhere (#6853)

## 0.9.3 (2021-12-28)

Use PEP 585 syntax wherever possible (#6717)

## 0.9.2 (2021-10-15)

Use lowercase tuple where possible (#6170)

## 0.9.1 (2021-10-12)

Add star to all non-0.1 versions (#6146)

## 0.9.0 (2021-09-19)

Update third-party package versions (#5996)

