## 2.5.2 (2022-05-19)

Import SQLAlchemy types in Flask-SQLAlchemy (#7861)

## 2.5.1 (2022-01-22)

Add __getattr__ to flask-sqlalchemy (#6993)

The SQLAlchemy class exposes dynamically classes of SQLAlchemy. The exact classes depend on used SQLAlchemy version.

## 2.5.0 (2022-01-20)

Add stubs for Flask-SQLAlchemy (#6946)

