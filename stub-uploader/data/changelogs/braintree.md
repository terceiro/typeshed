## 4.11.4 (2022-03-06)

Upgrade to stubtest with dunder pos only reverted (#7442)

## 4.11.3 (2022-01-31)

Upgrade black version (#7089)

## 4.11.1 (2021-10-12)

Add star to all non-0.1 versions (#6146)

