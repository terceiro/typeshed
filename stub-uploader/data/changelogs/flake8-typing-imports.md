## 1.11.3 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 1.11.2 (2022-01-08)

Use lowercase `type` everywhere (#6853)

## 1.11.0 (2021-12-10)

Add stubs for flake8-typing-imports (#6556)

