## 1.1.7 (2022-04-16)

Use `TypeAlias` where possible for type aliases (#7630)

## 1.1.6 (2022-04-05)

Mark many attributes as read-only properties (#7591)

## 1.1.4 (2021-12-28)

Use PEP 585 syntax wherever possible (#6717)

## 1.1.3 (2021-12-17)

Use stubtest 0.920 (#6589)

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>
Co-authored-by: Jelle Zijlstra <jelle.zijlstra@gmail.com>
Co-authored-by: Sebastian Rittau <srittau@rittau.biz>
Co-authored-by: Akuli <akuviljanen17@gmail.com>

## 1.1.2 (2021-10-12)

Add star to all non-0.1 versions (#6146)

