## 1.5.4 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 1.5.3 (2022-01-08)

Use lowercase `type` everywhere (#6853)

## 1.5.1 (2021-12-06)

Mark some fields as ClassVars (#6510)

## 1.5.0 (2021-12-03)

Add stubs for flake8-builtins (#6483)

