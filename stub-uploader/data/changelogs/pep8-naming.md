## 0.12.1 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 0.12.0 (2022-01-25)

Fix missing return type in pep8ext_naming (#7031)

Add stubs for pep8-naming (#7030)

