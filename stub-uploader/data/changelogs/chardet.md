## 4.0.4 (2022-04-27)

Drop Python 2 support from chardet (#7708)

## 4.0.2 (2021-12-28)

Use PEP 585 syntax wherever possible (#6717)

## 4.0.1 (2021-10-12)

Add star to all non-0.1 versions (#6146)

## 4.0.0 (2021-09-19)

Update third-party package versions (#5996)

