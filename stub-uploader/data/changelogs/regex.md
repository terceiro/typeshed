## 2021.11.10.5 (2022-05-11)

regex: accept buffers (#7680)

Similar to #7679

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>
Co-authored-by: Akuli <akuviljanen17@gmail.com>

## 2021.11.10.4 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 2021.11.10.3 (2022-02-23)

regex stubs: Add __getitem__ method to Match (#7372)

## 2021.11.10.2 (2022-01-07)

Update pyright (#6840)

## 2021.11.10.0 (2021-12-30)

Add regex stubs (#6713)

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>
Co-authored-by: Akuli <akuviljanen17@gmail.com>

