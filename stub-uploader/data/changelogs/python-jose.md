## 3.3.1 (2022-05-26)

Third-party stubs: fix several fictitious type aliases (#7958)

## 3.3.0 (2022-04-12)

Add stubs for Python-Jose (#7613)

