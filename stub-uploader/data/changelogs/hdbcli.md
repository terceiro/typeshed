## 2.10.7 (2022-04-16)

Third-party stubs: import from `collections.abc` where possible (#7637)

## 2.10.6 (2022-04-16)

Use `TypeAlias` where possible for type aliases (#7630)

## 2.10.5 (2022-01-20)

Use the Literal["foo", "bar"] syntax consistently (#6984)

## 2.10.4 (2022-01-08)

Use lowercase `type` everywhere (#6853)

## 2.10.2 (2021-12-28)

Use PEP 585 syntax wherever possible (#6717)

## 2.10.1 (2021-12-17)

Use stubtest 0.920 (#6589)

Co-authored-by: Alex Waygood <Alex.Waygood@Gmail.com>
Co-authored-by: Jelle Zijlstra <jelle.zijlstra@gmail.com>
Co-authored-by: Sebastian Rittau <srittau@rittau.biz>
Co-authored-by: Akuli <akuviljanen17@gmail.com>

## 2.10.0 (2021-12-09)

Add stubs for hdbcli (#6550)

hdbcli is the python dbapi for SAP HANA. Not all methods/attributes are 100% compatible with PEP 249.

